# UDDYS BBS


## Dependence

* [PHP](https://secure.php.net/) 7.1以上
* [CakePHP](https://book.cakephp.org/3.0/ja/index.html) 3.4以上

## Get Started

### 準備

composerをインストール

```
$ curl -sS https://getcomposer.org/installer | php -- --install-dir=bin
```

依存性を解決して必要なライブラリをインストール

```
$ composer install --dev
```
データベースに初期データを入れ込む

```
$ bin/cake migrations migrate
```

環境変数を設定

```
config/.envに環境変数を設定してください
DEBUG=true
DATABASE_URL="mysql://[user]:[password]@[host]/[database_name]?encoding=utf8mb4&timezone=Asia/Tokyo"
DATABASE_TEST_URL="mysql://[user]:[password]@[host]/[test_database_name]?encoding=utf8mb4&timezone=Asia/Tokyo"
DOMAIN="[host]:[port]"
S3_HOST="http://********"
RECAPTCH_SITEKEY="************"
RECAPTCH_SECRET="************"
```

### サーバー起動

以下のコマンドを実行するとブラウザで開発中のページが開きます。

```
$ bin/cake server
```
