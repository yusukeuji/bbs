<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserconfirmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserconfirmsTable Test Case
 */
class UserconfirmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserconfirmsTable
     */
    public $Userconfirms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.userconfirms',
        'app.users',
        'app.threads',
        'app.comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Userconfirms') ? [] : ['className' => UserconfirmsTable::class];
        $this->Userconfirms = TableRegistry::get('Userconfirms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Userconfirms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
