<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * ForgotPasswordExpiredDelete shell command.
 */
class UserConfirmsExpiredDeleteShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('UserConfirms');
    }
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {
        $date = date("Y-m-d H:i:s", strtotime("24 hours ago"));
        $conditions = ['created_at <=' => $date];
        $count = $this->UserConfirms->find()
            ->where($conditions)
            ->count();
        if($count > 0){
            $this->out("Target data is {$count}.");
            $this->UserConfirms->deleteAll($conditions);
        }
        $this->out("Finish");
    }
}

