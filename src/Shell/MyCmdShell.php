<?php

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;


class MyCmdShell extends Shell
{ 
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
    }

    public function main($num)
    {
        $data = $this->Users->get($num);
        $this->Users->delete($data);

    }

}

?>
