<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
//use Cake\I18n\Time;
//use Cake\ORM\TableRegistry;
//use Bakkerij\Notifier\Utility\NotificationManager;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
        public $name = 'Users';
        public $uses = array('User');
    public function initialize()
    {  
        
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
                'loginRedirect' => [
                  'controller' => 'Users',
                  'action' => 'login'
                ],
                'authorize' => ['Controller'],
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'username',
                            'password' => 'password',
                        ],
                         'scope' => ['Users.status' => 1]
                      ]
                ],
                'logoutRedirect' => [
                    'controller' => 'Threads',
                    'action' => 'index'
                ],
                'unauthorizedRedirect' => $this->referer()
                    ]);
    }

    public function isAuthorized($user)
    {
        if (isset($user['role']) && $user['role'] === 'admin') {
            return true;
        }
        return false;
    }
    //パスワードハッシュ化
    public function getSha1Hashkey($str="_sha1hashkey")
    {
        $key = sha1(uniqid() . mt_rand(1, 999999999) .$str);
        return $key;
    }
    //メール送信 
    public function sendMail($to, $subject, $values, $template)
    {
        if (is_array($values)) {
            $values['config'] = $this->config;
        } else {
            $values = [$values];
        }

        $mail = new Email('default');
        $mail->template($template)
            ->to($to)
            ->subject($subject)
            ->viewVars($values)
            ->send();
        return $mail;
    }
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {

    }
}
