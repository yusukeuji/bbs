<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use App\Handler\MyFbPersistentDataHandler;
use Facebook\Facebook;
use Abraham\TwitterOAuth\TwitterOAuth;
require __DIR__ . '/Facebook/autoload.php';
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add','login','signupConfirm','fbLoginCallback','fblogin','callback','twitter']);
        if($this->params['action'] == 'opauthComplete') {
            $this->Security->csrfCheck = false;
            $this->Security->validatePost = false;
        }
    }


    public function login()
    {
        if ($this->request->is('post')) { 
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->Flash->success('ログインしました');
                return $this->redirect(['controller' => 'threads']);
            }
            $this->Flash->error(__('ユーザー名またはパスワードが違います。(アカウントを有効にしてください。)'));
        }


        $fb = new Facebook([
                'app_id' => 1248848621907740,
                'app_secret' => 'a5f8d4964de44074d6faf251279c1417',
                'default_graph_version' => 'v2.2',
                'persistent_data_handler' => new MyFbPersistentDataHandler($this->request->session())
        ]);
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://uji.giginc.xyz/users/fbLoginCallback', $permissions);

        $this->set('fblogin',$loginUrl);
    }

    public function logout()
    {
        $this->Flash->success('ログアウトしました');
        return $this->redirect($this->Auth->logout());
    }

    public function index()
    {
        $user = $this->Auth->user('username');
        $userId = $this->Auth->user("id");
        $this->set('user',$user);
        $this->set('userId',$userId);
    }

    public function add()
    {

        $fb = new Facebook([
                'app_id' => 1248848621907740,
                'app_secret' => 'a5f8d4964de44074d6faf251279c1417',
                'default_graph_version' => 'v2.2',
                'persistent_data_handler' => new MyFbPersistentDataHandler($this->request->session())
        ]);
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://uji.giginc.xyz/users/fbLoginCallback', $permissions);

        
        $this->set('fblogin',$loginUrl);

        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $data = $this->request->getData();
            //既にユーザが存在しているかどうか
            $extuser = $this->Users->findByEmail($data['email'])->first();
            if ($extuser) {
                if ($extuser->status == '1') {
                    $this->Flash->error(__('このユーザは既に存在いたします。'));
                    $this->set(compact('user'));
                    return;
                } elseif ($extuser->status == '0') {
                    $user = $extuser;
                }
            }  
            //パスワード一致しているかどうか
            if($this->request->data('pass_check') === $this->request->data('password')) {
                $hash = $this->getSha1Hashkey('_signup');        
                $data['user_confirm']['hash'] = $hash;
                //アソシエーション
                $user = $this->Users->patchEntity($user, $data, ['associated' => [
                        'UserConfirms',
                ]]);
                if (!$user->errors()) {
                    $this->Users->save($user);
                    $this->sendMail($user->email, "{$user->username}様への登録確認", [
                            'name' => $user->username,                            
                            'hash' => $hash,
                            'limit_time' => date('Y/m/d H:i:s', strtotime('+1 day')),
                    ], 'Users/signup');
                    $this->Flash->success(__('頂いたメールアドレスに確認メールをお送りいたしました。ご確認頂きアカウントを有効にしてください。'));
                    $this->set(compact('user'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('登録できませんでした。大変お手数ですが、もう一度やり直してください。'));
                }
            } else {
                $this->Flash->error(__('パスワード確認の値が一致しません。'));
            }
        }
        $this->set(compact('user'));

    }

    public function signupConfirm($hash)
    {
        $this->loadModel('UserConfirms');
        if ($hash) {
            $user_confirm = $this->UserConfirms->find()
                ->contain(['Users'])
                ->where(['hash' => $hash])
                ->first();
            if ($user_confirm) {
                if ($user_confirm->user->status == '0') {
                    $data =['status' => '1'];
                }    
                $user = $this->Users->patchEntity($user_confirm->user, $data);
                $this->Users->save($user);
                $this->Flash->success(__('アカウントが有効になりました。'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('このアカウントは既に有効です。'));
                return $this->redirect(['action' => 'login']);
            }

        }
    }


    public function fbLoginCallback(){
        $fb = new Facebook([
                'app_id' => 1248848621907740,
                'app_secret' => 'a5f8d4964de44074d6faf251279c1417',
                'default_graph_version' => 'v2.2',
                'persistent_data_handler' => new MyFbPersistentDataHandler($this->request->session())
        ]);
        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        // Logged in
        echo '<h3>Access Token</h3>';
        var_dump($accessToken->getValue());
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken((string) $accessToken);
        $_SESSION['facebook_access_token'] = $longLivedAccessToken;

        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']); 
        try {
            $response = $fb->get('/me?locale=ja_JP');
            $fbuser = $response->getGraphUser();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph がエラーを返しました: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK がエラーを返しました: ' . $e->getMessage();
            exit;
        }
        $fid = $fbuser['id'];
        $fname = $fbuser['name'];


        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        echo '<h3>Metadata</h3>';
        var_dump($tokenMetadata);
        echo 'Facebook PHP SDK: '.$fbuser->getName().'としてログインしています。';
        echo $fid;
        echo $fname;

        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('1248848621907740'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }

            echo '<h3>Long-lived</h3>';
            var_dump($accessToken->getValue());
        }

        $_SESSION['fb_access_token'] = (string) $accessToken;
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');

        //  $this->redirect(['controller' => 'pages']);
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();

        $user->password =$fid;
        $user->username =$fname;
        $user->email ='facebook@gmail.com';
        $user->role ='admin';
        $user->status ='1';
        $this->Users->save($user);
        if ($this->request->is('post')){
            $user = $this->Auth->identify();
        }
        if($user){
            $this->Auth->setUser($user);
            $this->Flash->success('Facebookからのログインに成功しました');
            return $this->redirect($this->Auth->redirectUrl('http://uji.giginc.xyz/threads'));
        }else{
            echo "hello";
        }
    }
    public function Twitter(){
        // twitterのコネクション生成に必要な情報を設定
        $consumerKey = "LmMOuJG2wP88iuJuAybHHEfbh";
        $consumerSecret = "pOR8ghIm6wfrjcGvkdRSSTC7U9nn3tRrhC4ia4mxrO848vn4hz";
        $callback_url = "http://uji.giginc.xyz/users/callback";


        //TwitterOAuthのインスタンスを生成し、Twitterからリクエストトークンを取得する
        $connection = new TwitterOAuth($consumerKey,$consumerSecret);
        $request_token = $connection->oauth("oauth/request_token",
                ["oauth_callback" => $callback_url]
                );

        //リクエストトークンはcallback.phpでも利用するのでセッションに保存する
        $this->request->session()->write("oauth_token",$request_token['oauth_token']);
        $this->request->session()->write("oauth_token_secret",$request_token['oauth_token_secret']);

        // Twitterの認証画面へリダイレクト
        $url = $connection->url("oauth/authorize",
                ["oauth_token" => $request_token['oauth_token']]
                );
        header('Location: ' . $url);
        exit;
    }

    public function callback(){

        $consumerKey = "LmMOuJG2wP88iuJuAybHHEfbh";
        $consumerSecret = "pOR8ghIm6wfrjcGvkdRSSTC7U9nn3tRrhC4ia4mxrO848vn4hz";

        $oauth_token = $this->request->session()->read("oauth_token");
        $oauth_token_secret = $this->request->session()->read("oauth_token_secret");


        //Twitterからアクセストークンを取得する
        $connection = new TwitterOAuth($consumerKey,$consumerSecret,$oauth_token,$oauth_token_secret);

        $access_token = $connection->oauth("oauth/access_token",["oauth_verifier" => $_REQUEST['oauth_verifier']]);

        //※重要※ ここでもう一度アクセストークンを使って接続をし直す

        //OAuthトークンとシークレットも使って TwitterOAuth をインスタンス化
        $connection = new TwitterOAuth($consumerKey,$consumerSecret, $access_token['oauth_token'], $access_token['oauth_token_secret']);

        //ユーザー情報をGET
        $user = $connection->get("account/verify_credentials");

        $twuser = $user;
        $twpass = $twuser->id;
        $twname = $twuser->name;
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();
        $user->password =$twpass;
        $user->username =$twname;
        $user->email ='twitter@gmail.com';
        $user->role ='admin';
        $user->status ='1';
        $this->Users->save($user);
        if ($this->request->is('post')){
            $user = $this->Auth->identify();
        }
        if($user){
            $this->Auth->setUser($user);
            $this->Flash->success('Twitterからのログインに成功しました');
            return $this->redirect($this->Auth->redirectUrl('http://uji.giginc.xyz/threads'));
        }else{
            echo "hello";
        }
        var_dump($user->name);


    }
}
