<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * ImgProcess component
 */
class ImgProcessComponent extends Component
{
    function initialize(array $config) {
        $this->controller = $this->_registry->getController();
    }
    function save($request) {
        $file = $request->data['file_path'];
        $ext =  pathinfo($file['name'], PATHINFO_EXTENSION);
        $name = md5(uniqid(rand(), 1)).'.'.$ext;
        $request->data['file_ext'] = $ext;
        $request->data['file_size'] = $file['size'];
        $request->data['file_name'] = $name;
    }



    function generate($tmp_name, $thread) {
        move_uploaded_file($tmp_name, '../webroot/user_picture/'.$thread->file_name);
        $original_file = '../webroot/user_picture/'.$thread->file_name;;
        list($original_width, $original_height) = getimagesize($original_file);
        $thumb_width = 300;
        $thumb_height = round( $original_height * $thumb_width / $original_width );
        if($thread->file_ext === 'jpg') $original_image = imagecreatefromjpeg($original_file);
        if($thread->file_ext === 'png') $original_image = imagecreatefrompng($original_file);
        if($thread->file_ext === 'gif') $original_image = imagecreatefromgif($original_file);
        $thumb_image = imagecreatetruecolor($thumb_width, $thumb_height);
        imagecopyresized($thumb_image, $original_image, 0, 0, 0, 0,
                $thumb_width, $thumb_height,
                $original_width, $original_height);
        if($thread->file_ext === 'jpg') imagejpeg($thumb_image, '../webroot/user_picture.mini/'.$thread->file_name);
        if($thread->file_ext === 'png') imagepng($thumb_image, '../webroot/user_picture.mini/'.$thread->file_name);
        if($thread->file_ext === 'gif') imagegif($thumb_image, '../webroot/user_picture.mini/'.$thread->file_name);
    }
}
