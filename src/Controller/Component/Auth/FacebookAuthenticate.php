<?php
App::uses('BaseAuthenticate','Controller/Component/Auth');
App::import('Vendor','facebook',array('file'=>'facebook/src/facebook.php'));

class FacebookAuthenticate extends BaseAuthenticate {
    public $settings = array(
            'fields'=>array(
                'fbUserId'=>'fb_user_id',
                'fbToken'=>'fb_tokem',
                ),
            'userModel'=>'User',
            'fbScope'=>'publish_stream,email',
            );

    public function authenticate(CakeRequest $request,CakeResponse $response){
        CakeLog::write('debug', '[FacebookAuthenticate/authenticate] REQUEST');
        try{
            $facebook = new Facebook(array(
                        'appId'=>$this->settings['1248848621907740'],
                        'secret'=>$this->settings['a5f8d4964de44074d6faf251279c1417'],
                        'cookie'=>true
                        ));

            switch ($request->params['action']){
                case 'fblogin':
                    CakeLog::write('debug', '[FacebookAuthenticate/authenticate login] REQUEST');                   
                    $login_url = $facebook->getLoginUrl(array(
                                'redirect_uri'=>$this->settings['fbRedirect'],
                                'scope'=>$this->settings['fbScope'],
                                ));
                    $response->header('Location',$login_url);
                    $response->send();
                    break;
                case 'callback':
                    CakeLog::write('debug', '[FacebookAuthenticate/authenticate callback] REQUEST');            
                    return $this->_fb_update_user(
                            $user_id = $facebook->getUser(),
                            $facebook->getAccessToken()
                            );
                    break;              
            }
        } catch(OAuthException $e){
            CakeLog::write('debug', '[FacebookAuthenticate/authenticate OAuthException] REQUEST');                  
            debug($e);
        }
    }

    public function _fb_update_user($user_id,$token){
        CakeLog::write('debug', '[FacebookAuthenticate/_fb_update_user($user_id,$token)] REQUEST');                 
        $fields = $this->settings['fields'];
        $model_name = $this->settings['userModel'];

        $model = ClassRegistry::init($model_name);
        $user = $model->find(
                'first',
                array(
                    'conditions'=>array(
                        $model_name.'.'.$fields['fbUserId']=>$user_id
                        ),
                    'recursive'=>0
                    )
                );

        if(empty($user)||empty($user[$model_name])){
            CakeLog::write('debug', '[FacebookAuthenticate/_fb_update_user($user_id,$token) empty($user)||empty($user[$model_name])] REQUEST');                 
            $model->create();
            $user = array(
                    $model_name => array(
                        $fields['fbUserId']=>$user_id,
                        $fields['fbToken']=>$token,
                        ));
        }else{
            CakeLog::write('debug', '[FacebookAuthenticate/_fb_update_user($user_id,$token) else REQUEST');                 
            $user[$model_name][$fields['fbToken']]=$token;
        }
        return $model->save($user);
    }               
}



