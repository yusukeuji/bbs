<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[] paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Threads']
        ];
        $comments = $this->paginate($this->Comments);

        $this->set(compact('comments'));
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
                'contain' => ['Users', 'Threads']
        ]);

        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $thread_id = $this->request->getData('thread_id');
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $comment = $this->Comments->patchEntity($comment, $data);
            $comment->user_id = $this->Auth->user('id');
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('コメントを保存しました'));

                return $this->redirect("/threads/{$thread_id}");
            }
            $this->Flash->error(__('コメントを保存できませんでした'));
        }
        return $this->redirect("/threads/{$thread_id}");
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    { 
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->getData('id');
        $comment = $this->Comments->get($id);
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('コメントが削除されました'));
        } else {
            $this->Flash->error(__('失敗'));
        }

        return $this->redirect("/threads");
    }

}
