<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\validation\Validator;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use RuntimeException;

/**
 * Threads Controller
 *
 * @property \App\Model\Table\ThreadsTable $Threads
 *
 * @method \App\Model\Entity\Thread[] paginate($object = null, array $settings = [])
 */
class ThreadsController extends AppController
{
    private $users;
    public  $paginate =[
        'contain' => ['Comments','Users'],
        'limit'=> 5,
        'order'=>['id'=>'DESC']
    ];

        /**
         * Index method
         *
         * @return \Cake\Http\Response|null
         */

        public function initialize()
        {
            parent::initialize(); 
            $this->viewBuilder()->layout('default');
            $this->users = TableRegistry::get('Users');
        }  

        public function index($id=null)
        {
            $threads = $this->paginate($this->Threads);
            $this->set('threads', $threads);                
        }

        /* 
         * View method
         *
         * @param string|null $id Thread id.
         * @return \Cake\Http\Response|null
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */

        public function show($id)
        {
            $thread = $this->Threads->find()
                ->where(['Threads.id' => $id])
                ->first();
            $this->loadModel('Comments');
            $comments = $this->Comments->find()
                ->where(['Comments.thread_id' => $id])
                ->all();            
            // 新しいコメントを書き込む
            $comment = $this->Comments->newEntity();
            $this->set(compact(
                        'thread',
                        'comment',
                        'comments'
                        ));
        }

        /**
         * Add method
         *
         * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
         */

        public function add()    
        {
            $thread = $this->Threads->newEntity();           
            if($this->request->isPost()){  
                $thread->user_id = $this->Auth->user('id');             
                //file情報取得
                $file_path =  $_FILES["file_path"]['error'];         
                if($file_path == 1){
                    $this->Flash->error(__('画像サイズが大きすぎます（1.5M以下のファイルをアップロードしてください）'));
                    return  $this->redirect(['action' =>'add']);
                }
                $fileName =$this->request->data['file_path'];
                //fileを移動
                
                          
                move_uploaded_file($fileName['tmp_name'],'../webroot/user_picture/'.$fileName['name']);
                //text情報取得i

$h = 200; // リサイズしたい大きさを指定
$w = 200;

$file = '../webroot/user_picture/'.$fileName['name'];
list($original_w, $original_h, $type) = getimagesize($file);

// 加工前のファイルをフォーマット別に読み出す（この他にも対応可能なフォーマット有り）
switch ($type) {
    case IMAGETYPE_JPEG:
        $original_image = imagecreatefromjpeg($file);
        break;
    case IMAGETYPE_PNG:
        $original_image = imagecreatefrompng($file);
        break;
    case IMAGETYPE_GIF:
        $original_image = imagecreatefromgif($file);
        break;
    default:
        throw new RuntimeException('対応していないファイル形式です。: ', $type);
}

// 新しく描画するキャンバスを作成
$canvas = imagecreatetruecolor($w, $h);
imagecopyresampled($canvas, $original_image, 0,0,0,0, $w, $h, $original_w, $original_h);

$resize_path = '../webroot/user_picture_tmb/'. $fileName['name']; // 保存先を指定

switch ($type) {
    case IMAGETYPE_JPEG:
        imagejpeg($canvas, $resize_path);
        break;
    case IMAGETYPE_PNG:
        imagepng($canvas, $resize_path, 9);
        break;
    case IMAGETYPE_GIF:
        imagegif($canvas, $resize_path);
        break;
}
                $thread->text = $this->request->data['text']; 
                $thread->file_path = $fileName['name'];           
                if($this->Threads->save($thread)){
                    $this->Flash->success(__('画像が投稿されました'));
                    return  $this->redirect(['action' =>'index']); 
                }
                $this->Flash->error(__('画像の投稿に失敗しました。。'));
            }
            $this->set('thread',$this->Threads->newEntity());
        }
        /**
         * Edit method
         *
         * @param string|null $id Thread id.
         * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
         * @throws \Cake\Network\Exception\NotFoundException When record not found.
         */
        public function edit ($id = null)
        {
            $thread = $this->Threads->get($id,['contain'=>[]]);
            if ($this->request->is(['patch','post','put'])){
                $thread = $this->Threads->patchEntity($thread,$this->request->data);
                if($this->Threads->save($thread)){
                    $this->Flash->success(__('投稿が編集されました.'));
                    return $this->redirect(['action'=>'index']);
                }else{
                    $this->Flash->error(__('投稿が編集出来ませんでした. 再度お試し下さい.'));
                }
            }    
            $this->set(compact('thread'));
            $this->set('_serialize',['thread']);
        }   

        /**
         * Delete method
         *
         * @param string|null $id Thread id.
         * @return \Cake\Http\Response|null Redirects to index.
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function delete()
        {
            $this->request->allowMethod(['post','delete']);   
            $id = $this->request->getData('id');
            $thread =$this->Threads->get($id);
            if($this->Threads->delete($thread)){
                $this->Flash->success(_('削除完了'));
            }
            else{
                $this->Flash->error(_('失敗'));
            }

            return $this->redirect(['action' =>'index']);
        }  

        public function isAuthorized($user)
        {
            if ($this->request->getParam('action') === 'add') {
                return true;
            }

            if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {        
                $threadId = (int)$this->request->getParam('pass.0');
                if ($this->Threads->isOwnedBy($threadId, $user['id'])) {
                    return true;            
                }
                else {
                    $this->Flash->error(_('この記事の編集、削除権限がありません。'));
                    return false;
                }
            }
            return parent::isAuthorized($user);
        }    

}
