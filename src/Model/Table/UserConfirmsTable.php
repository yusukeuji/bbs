<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userconfirms Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Userconfirm get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userconfirm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Userconfirm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userconfirm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userconfirm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userconfirm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userconfirm findOrCreate($search, callable $callback = null, $options = [])
 */
class UserConfirmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_confirms');
        $this->setPrimaryKey('hash');

        $this->belongsTo('Users', [
                'foreignKey' => 'user_id',
        ]);
       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('hash', 'create')
            ->notEmpty('hash');

        return $validator;

    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['id'], 'Users'));

        return $rules;
    }
}

