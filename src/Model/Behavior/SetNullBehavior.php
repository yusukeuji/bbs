<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * SetNull behavior
 */
class SetNullBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    function beforeSave($event, $entity, $options) {
        foreach($this->_table->schema()->columns() as $hash => $val) {
            if($this->_table->schema()->isNullable($val) === true) {
                $fval = $entity->get($val);
                if ($fval === '' || empty($fval)) {
                    $entity->set($val, NULL);
                }
            }
        }
        return true;
    }
}
