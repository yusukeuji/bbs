<?php
namespace App\Handler;
use Facebook\PersistentData\PersistentDataInterface;

class MyFbPersistentDataHandler implements PersistentDataInterface
{
    private $_session = null;

    public function __construct($session)
    {
        $this->_session = $session;
    }


    public function get($key)
    {
        return $this->_session->read($key);
    }

    public function set($key, $value)
    {
        $this->_session->write($key, $value);
    }
}
