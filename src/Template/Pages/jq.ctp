<div id="loader-bg">
<div id="loading">
<img src="http://www.highexistence.com/wp-content/themes/hethen/images/loading.gif">
</div>
</div>
<div id="main-contents">
<style>
#box{
    display:none;
margin : 0 auto 0 auto;
         max-width:960px;
}
#box ul{
margin:0;
padding:0;
        list-style-type:none;
}
#box li{
padding: 8px 0 8px 0;
color:black;
      border-bottom:1px solid skyblue;
}
.header contents{
position:relative;
}
#box_btn{
position:;
top:0;
right:0;
      border-radius: 0 0 8px 8px ;
padding: 6px 20px 6px 20px;
         background-color:skyblue;
cursor:pointer;
}
p{
margin:5px;
}
div{
margin:5px;
}
.image2{
cursor:pointer;
}
#full img {
display: none;
}

#full img:first-child  {
display: inline;
}
#image{
width:300px;
height:300px;
}
#image4{
width:300px;
height:300px;
}

#back{
width:1000px;
height:600px;
}

.slideshow {
position: relative;
}


.slideshow img {
position: absolute;
left: 0;
top: 0;
opacity:0;
transition: 2s; /* フェードの時間(秒) */
}
.slideshow .show {
opacity: 1;
         z-index: 1;
}
.toTop {
display: inline-block;
background: rgba(73,233,255,0.5);
padding: 15px;
color: white;
cursor: pointer;
}
#wrapper{
display:flex;
        justify-content: center;
}

</style>

<div id="container">
<div class="col-md-12 col-sm-12">

<h2>--JavaScript 練習用--</h2>
<div id="box">
<ul>
<li>ホーム</li>
<li>ログイン</li>
<li>ログアウト</li>
<li>サポート</li>
<li>お問い合わせ</li>
</ul>
</div>
<div class="header-contents">
<div id="box_btn">(1)ーSlideの練習ー</div>

</div>
</div>

<div id="msg">(2)ー複数項目の選択ー</div>
<div>
<select id="sel1" size="5" multiple>
<option>mac</option>
<option>windows</option>
<option>linux</option>
<option>ios</option>
<option>android</option>
</select>
<button id="btn1">Click</button>
</div>
<p>(3)テキスト追加、表示</p>
<div id="msg2"></div>
<input type="text" id="text1" />
<button id="btn2">Click</button>

<p>(5)AJAX でデータ取得、表示</p>
<p id="msg4">message from ajax bro.....</p>
<button id="btn4">Click</button>

<p id="msg6">(6)AJAXでJSONなデータ取得、表示</p>
<input type="text" placeholder="0,1,2のどれか入力" id="text6">
<button id="btn6">submit</button>
<p>(6)画像縮小、拡大</p>
<div id ="frame">
<img id="image" src="/user_picture/top.jpeg">
</div>
<div><button id="btn7">Click</button></div>


<p>(７)画像変更</p>
<div id="full">
<img id="image" src="/user_picture/top.jpeg">
<img id="image" src="/user_picture/2017062810272120110317234442.png">
</div>


<p>(8)text fadeout</p>
<h3 id="fadeout">fadeoutします</h3>

<p>(9)背景画像</p>
<div id="content">
<ul>
<li><a href="http://uji.kgwsss.top/user_picture/top.jpeg"><img id="image" src="/user_picture/top.jpeg"></a></li>
<li><a href="http://uji.kgwsss.top/user_picture/2017062810272120110317234442.png"><img id="image"  src="/user_picture/2017062810272120110317234442.png"></a></li>
</ul>
</div>

<div id="content2">
<p id="back">背景画像表示場所</p>

<div id="slideshow" class="slideshow">
<img  id="image"  src="/user_picture/img01.jpg" alt="">
<img  id="image"  src="/user_picture/img02.jpg" alt="">
<img id="image" src="/user_picture/img03.jpg" alt="">
</div>
</div>
<p class="toTop"><i class="fa fa-arrow-up" aria-hidden="true"></i></p>

</div>
</div>
<img class="imgBox" height="226" width="300"   src="http://webcreatorbox.com/sample/images/beach.jpg" alt="">
<img class="imgBox" height="226" width="300"   src="http://webcreatorbox.com/sample/images/sunset.jpg" alt="">
</div>

<p class="c-btn codebtn1">1 解説を表示</p>
<div class="c-ctt codecontent1">
1 ここに表示/非表示される内容を記載
</div>

<p class="c-btn codebtn2">2 解説を表示</p>
<div class="c-ctt codecontent2">
2 ここに表示/非表示される内容を記載
</div>

<p class="c-btn codebtn3">3 解説を表示</p>
<div class="c-ctt codecontent3">
3 ここに表示/非表示される内容を記載
</div>

<div id ="wrapper">
<h1>Flex box 練習</h1>
<p>abcdefg</p>
<p>などなど<p>
</div>
