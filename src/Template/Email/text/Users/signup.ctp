<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
?>
<?= $name ?> 様

この度は、UDDYS　BBS　をご利用頂き、誠にありがとうございます。

下記のURLにアクセスして頂き、アカウント作成を完了させてください。
 http://uji.giginc.xyz/users/signup_confirm/<?= $hash ?>

※ 24時間以内にアクセスして頂けないと有効期限が切れてしまいます。
（有効期限：<?= $limit_time ?>）

また、アカウントの安全性を維持するため、このメールは他者に転送しないでください。

