<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="large-3 medium-4 small-4 columns">
    <li class="name">
      <h1><a href="/">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
</nav>
<div class="container">
  <div class="col-md-12 col-sm-12">
    <h1>編集</h1>
    <h1><img src="<?php echo ' /webroot/user_picture/'.$thread['file_path'];?>"></h1>
    <h1><?=$thread->text?></h1>

    <?=$this->Form->create($thread,['url'=>['action'=>'edit']], ['enctype' => 'multipart/form-data'])?>
    <?= $this ->Form->file('file_path')?>
    <?= $this->Form->textarea("text")?>
    <?=$this->Form->button(_("送信"),["class"=>"btn btn-primary"])?>
    <?=$this->Form->end() ?>
    <p><?=$this->Html->link('一覧に戻る',['action'=>'index'])?></p>
  </div>
</div>

