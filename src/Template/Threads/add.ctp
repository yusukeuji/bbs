<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="title-area large-3 medium-4 small-6 columns">
    <li class="name">
      <h1><a href="/" style="color:white;">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
  <div class="top-bar-section">
    <ul class="right">
      <li><?= $this->Html->link('マイページ',['controller'=>'users/'])?></li>
      <li><?= $this->Html->link('ログアウト',['controller'=>'users/logout'])?></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="col-md-12 col-sm-12"> 
   <h1>写真投稿</h1>
   <p><i class="fa fa-camera-retro fa-3x"></i></p> 
   <p>写真投稿場所</p>
   <body onload="initialize()">
     <div id="map" style="width: 640px; height: 320px; margin:0 auto;"></div><hr>
      <div>
         <input id="address" type="textbox" value="Tokyo">
         <input type="button" value="住所｜郵便番号" onclick="codeAddress()">        
      </div>
      <ul>
      <li style="list-style:none;">緯度: <span id="lat"></span></li>
      <li style="list-style:none;">経度: <span id="lng"></span></li>
      </ul>
    </body>
    <?= $this->Form->create($thread, ['enctype' => 'multipart/form-data']); ?>
    <div class="error"> <?=$this->Form->error('text') ?> </div>
    <?= $this ->Form->file('file_path')?>
    <?= $this->Form->textarea('text') ?>
    <?= $this->Form->button(_('送信'),["class"=>"btn btn-primary"]) ?>
    <?= $this->Form->end() ?><br>
    <p><?= $this->Html->link('写真一覧', ['action'=>'index'])?></p>
  </div>
</div>
