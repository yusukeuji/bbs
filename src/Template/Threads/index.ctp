<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="title-area large-3 medium-4 small-4 columns">
    <li class="name">
      <h1><a href="/" style="color:white;">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
  <div class="top-bar-section">
    <ul class="right">
      <li><?= $this->Html->link('マイページ',['controller'=>'users/'])?></li> 
     <li><?= $this->Html->link('ログアウト',['controller'=>'users/logout'])?></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="wrapper col-md-12 col-sm-12">
    <h1>写真一覧</h1><br/>
    <p><i class="fa fa-camera-retro fa-4x"></i></p>
     <h4><?= $this->Html->link('写真を投稿する',['action' => 'add'])?></h4>
      <?php foreach ($threads as $obj): ?> 
        <div class="row">
         <div class="col-md-4 col-sm-4">
               <?php if ($obj->users): ?>
               <?php foreach ($obj->users as $user):?>       
               <?= $user->id?>
               <?php endforeach; ?>
               <?php endif;?>
           <h5>本文：<?= $this->Html->link($obj['text'],['action' => 'show',$obj['id']])?></h5>
       </div>
       </div>
        <p> <img src="<?php echo 'webroot/user_picture_tmb/'.$obj['file_path'];?>"></p>
<?php if ($obj->comments): ?>
    <?php $count = 1; ?>
      <hr>
      <?php foreach ($obj->comments as $comment):?>
        <?= $comment->user_name?>
        <?= $comment->created_at?>
        <?= $comment->text?>
      <hr>
       <?php $count ++?>
       <?php if($count>3):
       break;
       endif;
       ?>
    <?php endforeach; ?>
  <?php endif;?>
       <div class="top-bar-section">
         <ul class="right">
           <p><?= $this->Html->link("コメントを書く",['action' => 'show',$obj['id']])?></p>
         </ul>
       </div>
       <?php endforeach; ?>
       <div class="paginator">
         <ul class="pagination">
           <?=$this->Paginator->numbers() ?>
         </ul>     
       <p><?= $this->Paginator->counter() ?></p>
    </div> 
  </div>
</div>
