<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="large-3 medium-4 small-4 columns">
    <li class="name">
       <h1><a href="/">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
</nav>

<div class="container">
  <div class="col-md-12 m-12">
    <h4><?= $thread->text ?></h4>
    <h1><img src="<?php echo ' /webroot/user_picture/'.$thread['file_path'];?>"></h1>
    <div class="top-bar-section">
    <ul class="right">
    <a href="/threads/edit/<?= $thread->id ?>">編集</a>   
    <?= $this->Form->postLink(__('削除'),  ['action' => 'delete'], [ 'data' => ['id' => $thread->id],'confirm' => __('削除しますか？')  ]);?>
    </ul>
    </div>
  <hr>
    <?php foreach($comments as $data): ?>
        名前: <?= $data->user_name?>   (<?= $data->created_at?>)<br>
        コメント：<?= nl2br($data->text)?>
   <div class="top-bar-section">
   <ul class="right">
   <?= $this->Form->postLink(__('削除'),  ['controller'=>'/comments/delete/'], [ 'data' => ['id' => $data->id],'confirm' => __('削除しますか？')  ]);?><br>
   </ul>
   </div>
   <hr>
    <?php endforeach;?>
      <h3>コメントする</h3>
      <?= $this->Form->create($comment, ['type' => 'post','url' => '/comments/add', ]);?>
        <?= $this->Form->hidden('thread_id', ['value' =>$thread->id])?>
        <?= $this->Form->input('user_name')?>
        <?= $this->Form->textarea('text') ?>
        <?= $this->Form->button(_('送信'),["class"=>"btn btn-primary"])?>
      <?= $this->Form->end() ?>
    <p><?= $this->Html->link('一覧に戻る', ['action'=>'index'])?></p>
  </div>
</div>
