<nav class="top-bar expanded" data-topbar role="navigation">
<ul class="large-3 medium-4 columns">
<li class="name">
<h1><a href="/" style="color:white;">UDDYS BBS for Travel Lovers</a></h1>
</li>
</ul>
<div class="top-bar-section">
<ul class="right">
<li><?= $this->Html->link('写真投稿',['controller'=>'threads'])?></li>
</ul>
</div>
</nav>

<h1>ユーザー情報</h1>
<div class="container">
  <div class="col-md-12 m-12">
<h2><?=$user?></h2>
    <div class="users form">
      <?= $this->Form->create($user,['url'=>['action'=>'edit']]) ?>
      <fieldset>
        <legend><?= __('編集') ?></legend>        
        <?= $this->Form->input('username',array(
                    'class' => 'form-control',
                    'div' =>false,
                    'placeholder' => '名前',
                    'label'=>false 
                    ))?>
        <?= $this->Form->input('email',array(
                    'class' => 'form-control',
                    'div' =>false,
                    'placeholder' => 'メールアドレス',
                    'label'=>false 
                    ))?>
        <?= $this->Form->input('password',array(
                    'class' => 'form-control',
                    'div' =>false,
                    'placeholder' => 'パスワード',
                    'label'=>false
                    )) ?>
        <?= $this->Form->input('role', ['options' => ['admin' => 'Admin', 'author' => 'Author']
        ]) ?>
        <?= $this->Form->button(__('Submit'),["class"=>"btn btn-primary"]); ?>
        <?= $this->Form->end() ?>     
     </fieldset>
     <p><?=$this->Html->link('一覧に戻る',['controller'=>'threads'])?></p>
    </div>
  </div>
</div>
