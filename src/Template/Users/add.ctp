<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="large-3 medium-4 columns">
    <li class="name">
      <h1><a href="/" style="color:white;">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
  <div class="top-bar-section">
    <ul class="right">
      <li><?= $this->Html->link('ログイン',['controller'=>'users/login'])?></li>
    </ul>
  </div>
</nav>
 <div class="container">
   <div class="col-md-12 m-12">
    <div class="users form">
      <?= $this->Form->create($user,['url' => '']); ?>
    <fieldset>
     <legend><?= __('サインアップ') ?></legend>
     <?= $this->Form->input('username',array(
                 'class' => 'form-control',
                 'div' =>false,
                 'placeholder' => '名前',
                 'label'=>false 
                 ))?>
     <?= $this->Form->input('email',array(
                 'class' => 'form-control',
                 'div' =>false,
                 'placeholder' => 'メールアドレス',
                 'label'=>false 
                 ))?>
     <?= $this->Form->input('password',array(
                 'class' => 'form-control',
                 'div' =>false,
                 'placeholder' => 'パスワード',
                 'label'=>false
                 )) ?>
      <?= $this->Form->input('pass_check',array(
                 'class' => 'form-control',
                 'div' =>false,
                 'placeholder' => 'パスワード再入力',
                 'type' => 'password',
                 'label'=>false
                 )); ?>
        <?= $this->Form->input('role', ['options' => ['admin' => 'Admin', 'author' => 'Author']]) ?>
        <?= $this->Form->button(__('Submit'),["class"=>"btn btn-primary"]); ?>
        <?= $this->Form->end() ?>
     </fieldset>
     </div>
      <p><a href="<?=$fblogin?>">facebookでログイン</a><br>
      <a href="http://uji.giginc.xyz/users/twitter">Twitterでログイン</a></p>
   </div>
 </div>
