<nav class="top-bar expanded" data-topbar role="navigation">
  <ul class="large-3 medium-4 columns">
    <li class="name">
      <h1><a href="/" style="color:white;">UDDYS BBS for Travel Lovers</a></h1>
    </li>
  </ul>
  <div class="top-bar-section">
    <ul class="right">
      <li><?= $this->Html->link('サインアップ',['controller'=>'users/add'])?></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="col-md-12 m-12">
    <div class="users form">
      <?= $this->Flash->render('auth') ?>
      <?= $this->Form->create() ?>
      <fieldset>
      <legend><?= __('ログイン') ?></legend>
      <?= $this->Form->input('username',array(
                  'div' => false,
                  'class' => 'form-control',
                  'label' => false,
                  'placeholder' => '名前'      
                  ))?>
      <?= $this->Form->input('password',array(
                  'div' => false,
                  'class' => 'form-control',
                  'label' => false,
                  'placeholder' => 'パスワード'       
                  )) ?>
      </fieldset>
      <?= $this->Form->button(__('Login'),["class"=>"btn btn-primary"]); ?>
      <?= $this->Form->end() ?>
    </div>
  </div>
 <p><a href="<?=$fblogin?>">facebookでログイン</a><br>
 <a href="http://uji.giginc.xyz/users/twitter">Twitterでログイン</a></p>
</div>
