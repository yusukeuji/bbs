<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>UDDY'S BBS</title>
        <!-- Required meta tags -->
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- jQuery, Tether -->
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>      
        <script>

        $(function(){
          
          $('#box_btn').on('click',function(){
            $('#box').slideToggle('fast');
          });
        
          $('#btn1').on('click',function(){
            var msg = 'selected: ';
            $("#sel1 option:selected").each(function(){
             msg += $(this).val()+" ";            
         });
          $('#msg').text(msg);
          });
          
          $('#btn2').on('click',function(){
          var obj =$('<p>'+$('#text1').val()+'</p>');
          $('#msg2').append(obj);
          });
        
          $('#btn3').on('click',function(){
          $('p[name=msg3]').wrapAll('<div></div>');

          });
        
          $('#btn4').on('click',function(){
          $('#msg4').load('index.html');                    
          });
         
           $('#btn6').on('click',function(){
           var id= $('#text6').val();
           $.getJSON('data.php',{id:id},function(response,status,xhr){
           var ol =$('<ul></ul>');
           ol.append('<li>名前:'+response.name+'</li>');
           ol.append('<li>メールアドレス:'+response.mail+'</li>');
           ol.append('<li>電話番号:'+response.tel+'</li>');
           $('#msg6').empty().append(ol);
           });        
           });

 
           $('#btn7').on('click',function(){
             $('#image').toggle(500);
           });
            
            
           $('#full img').click(function() {
               $(this).fadeOut('fast', function() {
               $(this).next('img').fadeIn();
               $('#full').append(this);
               });
           });    
           
           $('#fadeout').mouseover(function(){
                $('#fadeout').fadeOut();
           });
          

           $('ul li a').click(function(){
               return false;
           });
           $('ul li a').mouseover(function(){
            var bglink = $(this).attr("href");
             $("div#content2")
             .css({"background-image":"url("+bglink+")",'background-repeat': 'no-repeat'});
           });

           $(window).load(function() {
                   var img = $("#slideshow").children("img"), // 画像を取得
                   num = img.length, // 画像の数を数える
                   count = 0, // 現在何枚目を表示しているかのカウンター
                   interval = 5000; // 次の画像に切り替わるまでの時間(1/1000秒)

                   img.eq(0).addClass("show"); // 最初の画像にshowクラス付与

                   setTimeout(slide, interval); // slide関数をタイマーセット

                   function slide() {
                   img.eq(count).removeClass("show"); // 現在表示している画像からshowクラスを取り除く
                   count++; // カウンターを一個進める
                   if(count >= num) {
                   count = 0; // カウンターが画像の数より大きければリセット
                   }
                   img.eq(count).addClass("show"); // 次の画像にshowクラス付与
                   setTimeout(slide, interval); // 再びタイマーセット
                   }
                   });
    
                   $('.toTop').click(function () {
                       $('body,html').animate({
                       scrollTop: 0
                   }, 800,'linear');
                   return false;
                   });
                   
                   $(function() {
                           var h = $(window).height();
                           $('#main-contents').css('display','none');
                           $('#loader-bg ,#loader').height(h).css('display','block');
                           });

                   $(window).load(function () {
                           $('#loader-bg').delay(900).fadeOut(800);
                           $('#loader').delay(600).fadeOut(300);
                           $('#main-contents').css('display', 'block');
                           });

                  // 表示、非表示　for rooters 案件 
                   $(document).ready(function(){
                           $(".codecontent1").hide();
                           var pon1 = "close";
                           $(".codecontent2").hide();
                           var pon2 = "close";
                           $(".codecontent3").hide();
                           var pon3 = "close";
                           $(".codecontent4").hide();
                           //設置分
                           $(".codebtn1").click(function(){
                                   $(this).next(".codecontent1").slideToggle();
                                   if(pon1 == "close"){
                                   $(this).text("閉じる");
                                   pon1 = "open";
                                   }else{
                                   $(this).text("1 解説を表示");
                                   pon1 = "close";
                                   }
                                   });
                           $(".codebtn2").click(function(){
                                   $(this).next(".codecontent2").slideToggle();
                                   if(pon2 == "close"){
                                   $(this).text("閉じる");
                                   pon2 = "open";
                                   }else{
                                   $(this).text("2 解説を表示");
                                   pon2 = "close";
                                   }
                                   });
                           $(".codebtn3").click(function(){
                                   $(this).next(".codecontent3").slideToggle();
                                   if(pon3 == "close"){
                                   $(this).text("閉じる");
                                   pon3 = "open";
                                   }else{
                                   $(this).text("3 解説を表示");
                                   pon3 = "close";
                                   }
                                   });
                           //設置分
                   });
                   


        });
        </script>

        <!-- bootstrap framework -->
        <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <!-- Font -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
          <?= $this->Html->css('base.css')?>
          <?= $this->Html->css('cake.css')?>
 </head>
 <body>

 <?= $this->Flash->render() ?>
 <?= $this->fetch('content') ?>

 <hr>
 <footer class="footer bg-7 text-center">
 <p>&copy; 2017 UDDYS株式会社</p>
 </footer>

 </body>
 </html>
