<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
    <title>UDDY'S BBS</title>
    <!-- Required meta tags -->
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery, Tether -->
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    
    <script>
             
    var total = 10;  //カードの枚数
    var speed = 150;  //カードをめくる速
    var returnSec = 1000;  //めくったカードが元に戻る秒数
    var pic = [];  //各カードの番号を格納する配列
    var index;  //クリックしたカードの並び順
    var first = true;  //クリックしたカードが1枚目かどうかの判定用
    var card1;  //1枚目に引いたカードの番号
    var card2;  //2枚目に引いたカードの番号
    var pair = 0;  //正解したカードのペア数
   //カードを閉じる
   function cardClose(i,n)
   {
       $("#card li:eq("+i+")").stop().animate({ left: "75"}, speed);
       $("#card li:eq("+i+") img").stop().animate({ width: "0",height: "200px"}, speed,
           function(){
               n(i);
           }
       );
   }
   //表面を開く
   function omoteOpen()
   {
       $("#card li:eq("+index+") img").attr("src","/user_picture/card"+pic[index]+".jpg");
       $("#card li:eq("+index+") img").stop().animate({ width: "150px",height: "200px"}, speed);
       $("#card li:eq("+index+")").stop().animate({ left: "0"}, speed);
   }
   //裏面を開く
   function uraOpen(j)
   {
       $("#card li:eq("+j+") img").attr("src","/user_picture/card.png");
       $("#card li:eq("+j+") img").stop().animate({ width: "150px",height: "200px"}, speed);
       $("#card li:eq("+j+")").stop().animate({ left: "0"}, speed);
   }
   //クリックできないようにカードをロック
   function cardlock()
   {
       $("#card li:eq("+index+")").addClass("lock");
   }
   //全てのカードをロック
   function alllock()
   {
       $("#card li").addClass("lock");
   }
   //全てのカードをアンロック
   function unlock()
   {
       $("#card li").removeClass("lock");
   }
   //選んだ2枚のカードの正否
   function comparison()
   {
       if(card1==card2){  //2枚が同じカードであれば
           $("#card li:eq("+index+")").addClass("off");  //2枚目のカードのクリック判定を無効に
           $("#card li:eq("+index1+")").addClass("off");  //1枚目のカードのクリック判定を無効に
           pair++;  //ペア数を1増やす
           if(pair==total/2){  //ペアが全て見つかったら
               setTimeout(function()
                          {  //最後のカードがめくられた後にクリアー表示
                              alert("よくできましたーーー!!")
                          }, returnSec);
           }
       } else {  //2枚が違うカードであれば
           setTimeout(function()
                     {  //returnSecミリ秒後（カードをめくる動作が終わった後）に
                         cardClose(index,uraOpen);  //2枚目のカードを裏面に戻す
                         cardClose(index1,uraOpen);  //1枚目のカードを裏面に戻す
                     }, returnSec);
         }
       first = true;  //1枚目かどうかの判定を有効に
       card2 = 0;  //2枚目のカードの並び順をリセット
       setTimeout(function()
                 {
                      unlock();  //全てのカードの.lockを削除
                 }, returnSec+speed*2);
   }
    $(function()
    { 
          
       for(i=1; i<=total/2; i++){
           pic.push(i,i);
       }
       //配列の中身をランダムに並び替え
       pic.sort(function() 
               {
               return Math.random() - Math.random();
               });
       //カード画像の入ったliタグの生成
       for(i=1; i<=total; i++) {
           $("#card").append("<li><img src='/user_picture/card.png' width='150px' height='200px' ></li>");
       }
   
       $("#card li").click(function()
       {
           index = $("#card li").index(this);  //選択したカードの順番をindexに保存
           cardlock();  //選択したカードのクリックを無効にする関数
           cardClose(index,omoteOpen);  //カードを閉じ、表面を開く関数

           if(first == true){  //選択したカードが1枚目であれば
               index1 = index;  //カードの順番をindex1に保存
               card1= pic[index];  //並び順を基に表面の番号を配列から取り出しcard1に保存
               first = false;  //1枚目かどうかの判定を無効に
           } else {  //選択したカードが2枚目であれば
               alllock();  //全てのカードのクリックを無効にする関数
               card2 = pic[index];  //並び順を基に表面の番号を配列から取り出しcard2に保存
               comparison();  //card1とcard2を比べて正否の判定をする関数
           }
       });

  });
   </script>

        <!-- bootstrap framework -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
</head>
<body>
 <div class="container">
 <div class="wrapper col-md-12 col-sm-12">
<?= $this->fetch('content') ?>
</div>
</div>
</body>
</html>
