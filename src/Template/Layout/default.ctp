<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>UDDY'S BBS</title>
        <!-- Required meta tags -->
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- jQuery, Tether -->
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>      
        <!-- bootstrap framework -->
        <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <!-- Font -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
          <?= $this->Html->css('base.css')?>
          <?= $this->Html->css('cake.css')?>
    
        <style type="text/css">
            #map_canvas { height: 100% }
        </style>
        <!-- Google Maps APIを読み込む -->
        <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPyTEW6Y2yF6H-MkASjeeGVJMj8gIVnzc&callback=initMap">
        </script>
        <script>
            //Jqueryでサムネイル
            $(function(){
                $("p img").load(function(){
                    $(this).css({'width':'600','height':'400'});  
                 });
            });
        </script>
        <!-- initialize()関数を定義 -->
        <script type="text/javascript">
        var geocoder;
        var map;
  
       function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(35.7100627, 139.8107004);
            var mapOptions = {zoom: 8,center: latlng}
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
           
            map.addListener('click', function(e) {
            getClickLatLng(e.latLng, map);
            }); 
       } 

       function getClickLatLng(lat_lng, map) {
           document.getElementById('lat').textContent = lat_lng.lat();
           document.getElementById('lng').textContent = lat_lng.lng();

           var marker = new google.maps.Marker({
               position: lat_lng,
               map: map
           });
               map.panTo(lat_lng);
       }
      
       function codeAddress() {
            var address = document.getElementById('address').value;geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK'){
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({map: map,position: results[0].geometry.location});
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
       }
        </script>   
 </head>
 <body>

 <?= $this->Flash->render() ?>
 <?= $this->fetch('content') ?>

 <hr>
 <footer class="footer bg-7 text-center">
 <p>&copy; 2017 UDDYS株式会社</p>
 </footer>

 </body>
 </html>
